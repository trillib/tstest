import {createConnection} from "typeorm";
import {User} from "../Models/User";

export class DBAccessor {

    public initialize() {
        this.createUser("Timber", "Swagger", 25, user => {
            console.log(user)
        });
    }
    
    private createUser(firstName: string, lastName: string, age: number, onFinish: ((user: User) => void)){
        createConnection().then(async connection => {
            const user = new User();
            user.firstName = firstName;
            user.lastName = lastName;
            user.age = age;
            await connection.manager.save(user);
            let users = await connection.manager.find(User);
            onFinish(users[0])
        }).catch(error => console.log(error));
    }
}


